package logicpic.display;

import logicpic.tiles.Tiles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Display extends WindowAdapter {

    private static JFrame frame;
    private String title;
    private int WIDTH, HEIGHT;

    public Display(String title, int WIDTH, int HEIGHT){
        this.title = title;
        this.WIDTH = WIDTH;
        this.HEIGHT = HEIGHT;

        createDisplay();
    }

    public static void Reload(){
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;

        frame.add(Tiles.getPanellg(), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;

        frame.add(Tiles.getPanelpg(), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;

        frame.add(Tiles.getPanelld(), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;

        frame.add(Tiles.getPanel(), c);
        frame.pack();
    }

    private void createDisplay() {
        frame = new JFrame(title);
        frame.addWindowListener(this);
        frame.setSize(WIDTH, HEIGHT);
        frame.setMaximumSize(new Dimension(WIDTH,HEIGHT));
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.addKeyListener(new DisplayListener());
        frame.setFocusable(true);

        frame.setLayout(new GridBagLayout());
        Reload();
    }

    public void windowClosing(WindowEvent e) {
        int a = JOptionPane.showConfirmDialog(frame, "Jeżeli teraz wyjdziesz niezapisany stan gry zostanie stracony \n" +
                "Czy jesteś pewien?", "Wyjść z gry?", JOptionPane.YES_NO_OPTION);
        if (a == JOptionPane.YES_OPTION) {
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }
        public static JFrame getFrame() {
        return frame;
    }
}
