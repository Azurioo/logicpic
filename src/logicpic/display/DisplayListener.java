package logicpic.display;

import logicpic.main.Main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class DisplayListener implements KeyListener {
    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        int c = keyEvent.getKeyCode();
        if(c == 69){
            Main.MoveForward();
        }
        else if(c == 81){
            Main.MoveBackward();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }
}
