package logicpic.levels;

public class Level {
    private int[][] board;
    private int[][] colors;

    public Level(int[][] board, int[][] colors){
        this.board = board;
        this.colors = colors;
    }

    public int[][] getBoard() {
        return board;
    }

    public int[][] getColors() {
        return colors;
    }

    public void setBoard(int[][] board) {
        this.board = board;
    }

    public void setColors(int[][] colors) {
        this.colors = colors;
    }

    public void clear(){
        board = null;
        colors = null;
    }
}
