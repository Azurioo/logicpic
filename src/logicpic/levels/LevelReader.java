package logicpic.levels;

import java.io.BufferedReader;
import java.io.FileReader;

public class LevelReader {
    private int levelcount;
    private Level[] levels;

    public LevelReader(String path){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String line = reader.readLine();
            this.levelcount = Integer.parseInt(line);
            levels = new Level[levelcount];

            line = reader.readLine();
            for (int i = 0; i < levelcount; i++) {
                if(!line.equals("layout"))
                    throw new Exception("Błąd w składni pliku");

                line = reader.readLine();
                int [][] ArrayOfLayouts = new int[10][10];
                for (int y = 0; y < 10; y++) {
                    for (int x = 0; x < 10; x++) {
                        ArrayOfLayouts[y][x] = Integer.parseInt(line.split(" ")[x]);
                    }
                    line = reader.readLine();
                }

                if (!line.equals("colors"))
                    throw new Exception("Błąd w składni pliku");

                line = reader.readLine();
                int colorscount = Integer.parseInt(line);

                int count = 0;
                line = reader.readLine();
                int [][] ArrayOfColors = new int[colorscount][3];
                while (!line.equals("end")){
                    for (int x = 0; x < 3; x++) {
                        ArrayOfColors[count][x] = Integer.parseInt(line.split(" ")[x]);
                    }
                    count++;
                    line = reader.readLine();
                }
                line = reader.readLine();
                levels[i] = new Level(ArrayOfLayouts, ArrayOfColors);
            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    public Level[] getLevels() {
        return levels;
    }

    public int getLevelcount() {
        return levelcount;
    }
}
