package logicpic.main;

import logicpic.display.Display;

public class Game implements Runnable{

    private int width, height;
    private String title;

    public Game(String title, int width, int height){
        this.width = width;
        this.height = height;
        this.title = title;
    }

    public void run() {
        init();
    }

    private void init() {
        Main.create();
        Display display = new Display(title, width, height);
    }
}
