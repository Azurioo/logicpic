package logicpic.main;

import logicpic.display.Display;
import logicpic.levels.Level;
import logicpic.levels.LevelReader;
import logicpic.savefiles.History;
import logicpic.savefiles.Savefiles;
import logicpic.tiles.TileListener;
import logicpic.tiles.Tiles;

import javax.swing.*;
import java.awt.*;
import java.util.Random;
import java.util.Vector;

public class Main {

    private static boolean[][] solution;
    private static int SolCount = 0;
    private static String[][] topHints;
    private static String[][] bottomHints;
    private static int levelindex = 0;
    private static Level level;
    private static boolean won = false;

    public Main(){
        create();
    }

    public static void create(){
        Main.solution = generate();
        GenHints();
        Tiles.init(topHints,bottomHints);
    }

    public static void createnew(){
        Main.solution = generate();
        GenHints();
        Tiles.clear(topHints,bottomHints);
    }

    private static boolean[][] generate(){
        boolean[][] temp = new boolean[10][10];
        won = false;
        SolCount = 0;
        Random r = new Random();
        LevelReader levels = new LevelReader("resources/levels/levels.datapack");
        int last = levelindex;

        do {
            levelindex = r.nextInt(levels.getLevelcount());
        }while (levelindex == last);

        level = levels.getLevels()[levelindex];

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(level.getBoard()[i][j] > 0) {
                    temp[i][j] = true;
                    SolCount++;
                }else {
                    temp[i][j] = false;
                }
            }
        }
        return temp;
    }

    private static void GenHints(){
        int count = 0;
        boolean gap = false;
        boolean first = true;
        int[][] temptop = new int[5][10];

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 10; j++) {
                temptop[i][j] = 0;
            }
        }

        for (int i = 0; i < 10; i++) {
            count = 4;
            gap = false;
            first = true;
            for (int j = 9; j >= 0; j--) {
                if(solution[j][i] && !gap){
                    temptop[count][i]++;
                    first = false;
                }
                else if(solution[j][i] && gap){
                    count--;
                    temptop[count][i]++;
                    gap = false;
                }
                else{
                    if(!first)
                        gap = true;
                }
            }
        }

        int[][] tempbottom = new int[10][5];

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                tempbottom[i][j] = 0;
            }
        }

        for (int i = 0; i < 10; i++) {
            count = 4;
            gap = false;
            first = true;
            for (int j = 9; j >= 0; j--) {
                if(solution[i][j] && !gap){
                    tempbottom[i][count]++;
                    first = false;
                }
                else if(solution[i][j] && gap){
                    count--;
                    tempbottom[i][count]++;
                    gap = false;
                }
                else{
                    if(!first)
                        gap = true;
                }
            }
        }

        topHints = new String[5][10];
        bottomHints = new String[10][5];

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 10; j++) {
                topHints[i][j] = Integer.toString(temptop[i][j]);
            }
        }
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                bottomHints[i][j] = Integer.toString(tempbottom[i][j]);
            }
        }

    }

    public static void Load(int[][] solution, boolean[][] board, Vector<Integer> moves, int moves_count, int[][] colors){
        int tiles_count = 0;
        SolCount = 0;
        Tiles.clear(topHints,bottomHints);
        History.clear();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(board[i][j]) {
                    Tiles.getBoard()[i][j].setTriggered(true);
                    Tiles.getBoard()[i][j].setBackground(Color.BLACK);
                    tiles_count++;
                }
                else {
                    Tiles.getBoard()[i][j].setTriggered(false);
                    Tiles.getBoard()[i][j].setBackground(Color.WHITE);
                }
                if(Main.solution[i][j] = solution[i][j] > 0)
                    SolCount++;
            }
        }
        History.insertMoves(moves,moves.size());
        History.setCurrent(moves_count);
        TileListener.setCount(tiles_count);
        Main.setWon(false);
        GenHints();
        Tiles.reloadHints(topHints,bottomHints);
        level = new Level(solution,colors);
        Display.Reload();
    }

    public static void MoveForward(){
        int[] temp = History.moveForward();
        if(temp == null || won)
            return;
        if(Tiles.getBoard()[temp[0]][temp[1]].isTriggered()){
            Tiles.getBoard()[temp[0]][temp[1]].setTriggered(false);
            Tiles.getBoard()[temp[0]][temp[1]].setBackground(Color.WHITE);
            TileListener.setCount(TileListener.getCount() - 1);
        }else{
            Tiles.getBoard()[temp[0]][temp[1]].setTriggered(true);
            Tiles.getBoard()[temp[0]][temp[1]].setBackground(Color.BLACK);
            TileListener.setCount(TileListener.getCount() + 1);
        }
    }

    public static void MoveBackward(){
        int[] temp = History.moveBackward();
        if(temp == null || won)
            return;
        if(Tiles.getBoard()[temp[0]][temp[1]].isTriggered()){
            Tiles.getBoard()[temp[0]][temp[1]].setTriggered(false);
            Tiles.getBoard()[temp[0]][temp[1]].setBackground(Color.WHITE);
            TileListener.setCount(TileListener.getCount() - 1);
        }else{
            Tiles.getBoard()[temp[0]][temp[1]].setTriggered(true);
            Tiles.getBoard()[temp[0]][temp[1]].setBackground(Color.BLACK);
            TileListener.setCount(TileListener.getCount() + 1);
        }
    }

    public static void check(){
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(!(solution[i][j] == Tiles.getBoard()[i][j].isTriggered())){
                    return;
                }
            }
        }
        //Win condition
        won = true;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int color = level.getBoard()[i][j];
                Tiles.getBoard()[i][j].setBackground(new Color(level.getColors()[color][0], level.getColors()[color][1] ,level.getColors()[color][2]));
            }
        }
    }

    public static boolean[][] getSolution() {
        return solution;
    }

    public static int getSolCount() {
        return SolCount;
    }

    public static boolean isWon() {
        return won;
    }

    public static void setWon(boolean won) {
        Main.won = won;
    }

    public static void Save() {
        boolean[][] temp = new boolean[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                temp[i][j] = Tiles.getBoard()[i][j].isTriggered();
            }
        }
        Savefiles.SaveToFile(level.getBoard(),temp,History.getMoves(),History.getCurrent(),level.getColors());
    }

    public static void Help(){
        JOptionPane.showMessageDialog(Display.getFrame(), "Instrukcja: \n " +
                "Gra polega na tym aby zaznaczyć prawidłowe bloki tworząc obrazek. \n " +
                "Po lewej stronie i nad plansza znajdują się wskazówki jak zaznaczać bloki \n " +
                "Liczba oznacza ile bloków sąsiadujących ze soba w lini: \n " +
                "- pionowej - jeżeli liczba jest nad plansza \n " +
                "- poziomej - jeżeli liczba znajduję się po lewej stronie planszy \n " +
                "Jezeli jest wiecej niż jedna liczba w lini oznacza to że na plaszy będzie wiecej \n" +
                "oznacza to, że w odopowiadającej lini na planszy będzie wiecej niz 1 grupa sąsiadujących bloków, \n " +
                "gdzie każda grupa jest wielkości odpowiadającej swojej liczbie. \n " +
                "Przerwy między grupami nie są jednakowe i trzeba je dopasować do innych lini. \n " +
                "Jeżeli wszystko się powiedzie obrazek nabierze kolorów! \n " +
                "Przyciski po lewej stronie planszy: \n" +
                "- New - zacznij gre od nowa z nową planszą \n" +
                "- Hint - zaznaczy albo odznaczy jeden blok aby być krok bliżej do rozwiazania łamigłówki \n" +
                "- Clear - wyczyści plansze nie zmieniając jej \n" +
                "- Save i Load Game - odpowiadają za zapisywanie i wczytywanie aktualnej gry \n" +
                "Podczas gry można cofać i ponawiać wykone ruchy klikając odpowiednio Q lub E na klawiaturze \n" +
                "Powodzenia!");
    }
}
