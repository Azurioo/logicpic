package logicpic.menu;

import javax.swing.*;

public class MenuButton extends JButton {

    private int index;

    public MenuButton(String text){
        super(text);
    }
    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
}
