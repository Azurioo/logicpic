package logicpic.menu;

import logicpic.display.Display;
import logicpic.main.Main;
import logicpic.savefiles.History;
import logicpic.savefiles.Savefiles;
import logicpic.tiles.TileListener;
import logicpic.tiles.Tiles;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
        if(((MenuButton) e.getSource()).getIndex() == 0) Hint();
        else if(((MenuButton) e.getSource()).getIndex() == 1) Clear();
        else if(((MenuButton) e.getSource()).getIndex() == 2) Gen();
        else if(((MenuButton) e.getSource()).getIndex() == 3) Save();
        else if(((MenuButton) e.getSource()).getIndex() == 4) Load();
        else Help();
    }

    private void Load() {
        Savefiles.LoadFromFile();
    }

    private void Save() {
        Main.Save();
    }

    private void Help() {
        Main.Help();
    }

    private void Gen() {
        History.clear();
        Main.createnew();
        TileListener.setCount(0);
        Main.setWon(false);
        Display.Reload();
    }

    private void Clear() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Tiles.getBoard()[i][j].setTriggered(false);
                Tiles.getBoard()[i][j].setBackground(Color.WHITE);
            }
        }
        Main.setWon(false);
        TileListener.setCount(0);
    }

    private void Hint(){
        if(Main.isWon())
            return;
        int[] hint = findHint();
        if(hint == null)
            return;
        int i = hint[0];
        int j = hint[1];
        if(Tiles.getBoard()[i][j].isTriggered()){
            Tiles.getBoard()[i][j].setTriggered(false);
            Tiles.getBoard()[i][j].setBackground(Color.WHITE);
            TileListener.setCount(TileListener.getCount() - 1);
        }
        else {
            Tiles.getBoard()[i][j].setTriggered(true);
            Tiles.getBoard()[i][j].setBackground(Color.BLACK);
            TileListener.setCount(TileListener.getCount() + 1);
        }
        if(TileListener.getCount() == Main.getSolCount())
            Main.check();
        String[] temp = Tiles.getBoard()[i][j].getPos().split(" ");
        History.addMove(Integer.parseInt(temp[0]),Integer.parseInt(temp[1]));
    }

    private int[] findHint(){
        int[] found = new int[2];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(!Tiles.getBoard()[i][j].isTriggered() && Main.getSolution()[i][j] || Tiles.getBoard()[i][j].isTriggered() && !Main.getSolution()[i][j]){
                    found[0] = i;
                    found[1] = j;
                    return found;
                }
            }
        }
        return null;
    }
}
