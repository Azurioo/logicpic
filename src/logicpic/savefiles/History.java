package logicpic.savefiles;

import java.util.Vector;

public class History {

    private static Vector<Integer> moves = new Vector<Integer>();
    private static int current = 0;

    public static void addMove(int x, int y){
        for (int i = moves.size() - 1; i >= current; i--) {
            moves.removeElementAt(i);
        }
        moves.add(x);
        moves.add(y);
        current += 2;
    }

    public static void setCurrent(int current) {
        History.current = current;
    }

    public static int[] moveBackward(){
        if (current - 2 < 0)
            return null;

        current -= 2;
        int[] temp = new int[2];
        temp[0] = moves.get(current);
        temp[1] = moves.get(current+1);
        return temp;
    }

    public static int[] moveForward(){
        if(current + 2 > moves.size())
            return null;

        int[] temp = new int[2];
        temp[0] = moves.get(current);
        temp[1] = moves.get(current+1);
        current += 2;
        return temp;
    }

    public static void clear(){
        current = 0;
        moves.clear();
    }

    public static Vector<Integer> getMoves() {
        return moves;
    }

    public static int getCurrent() {
        return current;
    }

    public static void insertMoves(Vector<Integer> moves, int current){
        History.moves = moves;
        History.current = current;
    }
}
