package logicpic.savefiles;

import logicpic.display.Display;
import logicpic.main.Main;

import javax.swing.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class Savefiles {

    public static void SaveToFile(int[][] solution, boolean[][] board, Vector<Integer> moves, int moves_count, int[][] colors){
        try {
            String path = "resources/savefiles/save-";
            Date date = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
            String strDate = dateFormat.format(date);
            String extension = ".save";
            String fullpath = path + strDate + extension;

            FileWriter writer = new FileWriter(fullpath);
            PrintWriter printtofile = new PrintWriter(writer);
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    printtofile.print(solution[i][j]);
                    if(j!= 9)
                        printtofile.print(" ");
                }
                printtofile.println();
                for (int j = 0; j < 10; j++) {
                    printtofile.print(board[i][j] ? "1" : "0");
                    if(j!= 9)
                        printtofile.print(" ");
                }
                printtofile.println();
            }

            printtofile.print(colors.length);
            printtofile.println();

            for (int[] color : colors) {
                for (int j = 0; j < 3; j++) {
                    printtofile.print(color[j]);
                    if(j != 2)
                        printtofile.print(" ");
                }
                printtofile.println();
            }

            printtofile.print("end");
            printtofile.println();
            printtofile.print(moves_count);
            printtofile.println();

            moves.forEach(printtofile::println);

            printtofile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void LoadFromFile(){
        try {
            JFileChooser fileChooser = new JFileChooser(new File("resources/savefiles/"));
            fileChooser.setFileFilter(new FileTypeFilter(".save", "Savefile"));
            int result = fileChooser.showOpenDialog(Display.getFrame());
            if(result == JFileChooser.CANCEL_OPTION)
                return;


            BufferedReader reader = new BufferedReader(new FileReader(fileChooser.getSelectedFile()));
            String line = reader.readLine();

            int [][] soultion = new int[10][10];
            boolean [][] board = new boolean[10][10];
            for (int y = 0; y < 10; y++) {
                for (int x = 0; x < 10; x++) {
                    soultion[y][x] = Integer.parseInt(line.split(" ")[x]);
                }
                line = reader.readLine();
                for (int x = 0; x < 10; x++) {
                    board[y][x] = Integer.parseInt(line.split(" ")[x]) > 0;
                }
                line = reader.readLine();
            }

            int colorscount = Integer.parseInt(line);

            int count = 0;
            line = reader.readLine();
            int [][] ArrayOfColors = new int[colorscount][3];
            while (!line.equals("end")){
                for (int x = 0; x < 3; x++) {
                    ArrayOfColors[count][x] = Integer.parseInt(line.split(" ")[x]);
                }
                count++;
                line = reader.readLine();
            }

            line = reader.readLine();
            int moves_count = Integer.parseInt(line);
            line = reader.readLine();

            Vector<Integer> moves = new Vector<>();
            while (line != null){
                moves.add(Integer.parseInt(line));
                line = reader.readLine();
            }

            Main.Load(soultion,board,moves,moves_count, ArrayOfColors);
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}
