package logicpic.tiles;

import javax.swing.*;

public class Board extends JButton {

    private boolean triggered = false;
    private String pos;
    public Board(String pos){
        this.pos = pos;
    }
    public boolean isTriggered() {
        return triggered;
    }
    public void setTriggered(boolean triggered) {
        this.triggered = triggered;
    }
    public String getPos() {
        return pos;
    }
}
