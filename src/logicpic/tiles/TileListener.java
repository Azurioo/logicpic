package logicpic.tiles;

import logicpic.main.Main;
import logicpic.savefiles.History;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TileListener implements ActionListener {

    private static int count = 0;

    public void actionPerformed(ActionEvent e) {
        if(Main.isWon())
            return;
        if (((Board) e.getSource()).isTriggered()) {
            ((Board) e.getSource()).setTriggered(false);
            ((Board) e.getSource()).setBackground(Color.WHITE);
            TileListener.count--;
        }
        else {
            ((Board) e.getSource()).setTriggered(true);
            ((Board) e.getSource()).setBackground(Color.BLACK);
            TileListener.count++;
        }
        if(TileListener.count == Main.getSolCount())
            Main.check();
        String[] temp = ((Board) e.getSource()).getPos().split(" ");
        History.addMove(Integer.parseInt(temp[0]),Integer.parseInt(temp[1]));
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        TileListener.count = count;
    }
}
