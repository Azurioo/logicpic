package logicpic.tiles;

import logicpic.menu.MenuButton;
import logicpic.menu.MenuListener;

import javax.swing.*;
import java.awt.*;

public class Tiles{

    private static JPanel panel;
    private static JPanel panellg;
    private static JPanel panelpg;
    private static JPanel panelld;
    private static Board[][] board = new Board[10][10];
    private static JLabel[][] topLabel = new JLabel[5][10];
    private static JLabel[][] bottomLabel = new JLabel[10][5];

    public static void init(String[][] topHints, String[][] bottomHints){

        panel = new JPanel();
        panellg = new JPanel();
        panelpg = new JPanel();
        panelld = new JPanel();

        panellg.setLayout(new GridLayout(6,1));
        panellg.setFocusable(false);

        MenuButton Hint = new MenuButton("HINT");
        Hint.setIndex(0);
        Hint.setPreferredSize(new Dimension(200, 200/6));
        Hint.setFocusable(false);
        Hint.addActionListener(new MenuListener());

        MenuButton Clear = new MenuButton("CLEAR");
        Clear.setIndex(1);
        Clear.setPreferredSize(new Dimension(200, 200/6));
        Clear.setFocusable(false);
        Clear.addActionListener(new MenuListener());

        MenuButton New = new MenuButton("NEW");
        New.setIndex(2);
        New.setPreferredSize(new Dimension(200, 200/6));
        New.setFocusable(false);
        New.addActionListener(new MenuListener());

        MenuButton Save = new MenuButton("SAVE GAME");
        Save.setIndex(3);
        Save.setPreferredSize(new Dimension(200, 200/6));
        Save.setFocusable(false);
        Save.addActionListener(new MenuListener());

        MenuButton Load = new MenuButton("LOAD GAME");
        Load.setIndex(4);
        Load.setPreferredSize(new Dimension(200, 200/6));
        Load.setFocusable(false);
        Load.addActionListener(new MenuListener());

        MenuButton Help = new MenuButton("HELP");
        Help.setIndex(5);
        Help.setPreferredSize(new Dimension(200, 200/6));
        Help.setFocusable(false);
        Help.addActionListener(new MenuListener());

        panellg.add(New);
        panellg.add(Hint);
        panellg.add(Clear);
        panellg.add(Save);
        panellg.add(Load);
        panellg.add(Help);

        panel.setLayout(new GridLayout(10, 10));
        panel.setFocusable(false);
        panel.setMinimumSize(new Dimension(400, 400));

        panelpg.setLayout(new GridLayout(5, 10));
        panelpg.setFocusable(false);
        panelpg.setMinimumSize(new Dimension(400, 200));

        panelld.setLayout(new GridLayout(10, 5));
        panellg.setFocusable(false);
        panelld.setMinimumSize(new Dimension(200, 400));

        for(int i = 0; i < 10; i++)
        {
            for(int j = 0; j < 10; j++)
            {
                board[i][j] = new Board(i + " " + j);
                board[i][j].setFocusable(false);
                board[i][j].setBackground(Color.WHITE);
                board[i][j].setPreferredSize(new Dimension(40,40));
                board[i][j].setMaximumSize(new Dimension(40,40));
                board[i][j].setFocusPainted(false);
                board[i][j].addActionListener(new TileListener());
                panel.add(board[i][j]);
            }
        }

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 10; j++) {
                topLabel[i][j] = new JLabel(topHints[i][j].equals("0") ? " " : topHints[i][j], SwingConstants.CENTER);
                topLabel[i][j].setBackground(Color.WHITE);
                topLabel[i][j].setMinimumSize(new Dimension(40,40));
                topLabel[i][j].setPreferredSize(new Dimension(40,40));
                panelpg.add(topLabel[i][j]);
            }
        }
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                bottomLabel[i][j] = new JLabel(bottomHints[i][j].equals("0") ? " " : bottomHints[i][j], SwingConstants.CENTER);
                bottomLabel[i][j].setBackground(Color.WHITE);
                bottomLabel[i][j].setMinimumSize(new Dimension(40,40));
                bottomLabel[i][j].setPreferredSize(new Dimension(40,40));
                panelld.add(bottomLabel[i][j]);
            }
        }
    }

    public static Board[][] getBoard(){
        return board;
    }

    public static JPanel getPanel() {
        return panel;
    }

    public static JPanel getPanellg() {
        return panellg;
    }

    public static JPanel getPanelpg() {
        return panelpg;
    }

    public static JPanel getPanelld() {
        return panelld;
    }

    public static void clear(String[][] topHints, String[][] bottomHints){
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                board[i][j].setBackground(Color.WHITE);
                board[i][j].setTriggered(false);
            }
        }
        reloadHints(topHints,bottomHints);
    }

    public static void reloadHints(String[][] topHints, String[][] bottomHints){
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                bottomLabel[i][j].setText(bottomHints[i][j].equals("0") ? " " : bottomHints[i][j]);
            }
        }
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 10; j++) {
                topLabel[i][j].setText(topHints[i][j].equals("0") ? " " : topHints[i][j]);
            }
        }
    }
}